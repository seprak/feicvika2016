/*
 *  FEI prednaky 2016
 */
package sk.vsprk.fei.c4.ex1.impl;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import sk.vsprk.fei.c4.ex1.FileParser;

/**
 *
 * @author vsprk
 */
public class BasicFileParser implements FileParser {

    public String readFile(String fileName) {
        StringBuffer strBuff = new StringBuffer();
        BufferedReader br = null;
        FileReader fr = null;

        try {

            fr = new FileReader(fileName);
            br = new BufferedReader(fr);

            String sCurrentLine;

            br = new BufferedReader(new FileReader(fileName));

            while ((sCurrentLine = br.readLine()) != null) {
                strBuff.append(sCurrentLine);
            }

        } catch (IOException e) {

            e.printStackTrace();

        } finally {

            try {

                if (br != null) {
                    br.close();
                }

                if (fr != null) {
                    fr.close();
                }

            } catch (IOException ex) {

                ex.printStackTrace();

            }

        }
        return strBuff.toString();

    }

    public void print(String data, OutputStream out) {
        PrintWriter writer = new PrintWriter(out);
        try {
            writer.print(data);
            writer.flush();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            writer.close();
        }
    }

}
