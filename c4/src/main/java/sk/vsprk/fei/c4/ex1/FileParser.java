package sk.vsprk.fei.c4.ex1;

import java.io.OutputStream;

public interface FileParser {

	public String readFile(String fileName);
	
	public void print(String data, OutputStream out);
}
