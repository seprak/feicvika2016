# Citanie dat a ukladanie do DB

## Vytvorte aplikaciu ktora nacita data zo suboru a ulozi do DB

- pouzite spring-boot-starter-data-jpa  [http://spring.io/guides/gs/accessing-data-jpa/]
- Vytvorte triedu Employee ktora predstavuje zamestnanca vo firme, a implementujte EmployeeService ktora bude obsahovat funkcionalitu na spravu zamestnancov.
- Nacitajte zamestanancov zo suboru, a pomocou triedy Employee ulozte do DB
- Implementujte metody ktore z DB vyberu zamestnanca podla mena, priezviska alebo platu.
