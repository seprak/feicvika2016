/*
 *  FEI prednaky 2016
 */
package sk.vsprk.fei.c4.ex1.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import sk.vsprk.fei.c4.ex1.FileParser;

/**
 *
 * @author vsprk
 */
public class ScannerFileParserr implements FileParser {

    public String readFile(String fileName) {
        String out = null;
        try (Scanner scan = new Scanner(new File(fileName))) {
            out = scan.useDelimiter("\\Z").next();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ScannerFileParserr.class.getName()).log(Level.SEVERE, null, ex);
        }
        return out;
    }

    public void print(String data, OutputStream out) {
        PrintWriter writer = new PrintWriter(out);
        try {
            writer.print(data);
            writer.flush();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            writer.close();
        }
    }    
}
