/*
 *  FEI prednaky 2016
 */
package sk.vsprk.fei.c4.ex2.impl;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import sk.vsprk.fei.c4.ex2.Employee;

/**
 *
 * @author vsprk
 */
public interface EmployeeRepository extends CrudRepository<Employee, Long> {
    
    List<Employee> findByLastName(String lastName);
}
