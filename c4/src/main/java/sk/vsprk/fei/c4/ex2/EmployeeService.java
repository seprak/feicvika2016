package sk.vsprk.fei.c4.ex2;

import java.util.List;

public interface EmployeeService {

	public void addEmployee(Employee employee);
	
	public void deleteEmployee(Employee employee);
	
	public List<Employee> findAllEmployees();
	
	public Employee getEmployee(String surname); // Je toto ok?
	
	
}
