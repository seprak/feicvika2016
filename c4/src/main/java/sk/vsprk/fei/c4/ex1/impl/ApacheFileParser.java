/*
 *  FEI prednaky 2016
 */
package sk.vsprk.fei.c4.ex1.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Component;
import sk.vsprk.fei.c4.ex1.FileParser;

/**
 *
 * @author vsprk
 */
@Component
public class ApacheFileParser implements FileParser {

    @Override
    public String readFile(String fileName) {
        try {
            return IOUtils.toString(new FileInputStream(new File(fileName)), Charset.forName("UTF-8"));
        } catch (IOException ex) {
            Logger.getLogger(ApacheFileParser.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public void print(String data, OutputStream out) {
        try {
            IOUtils.write(data, out, Charset.defaultCharset());
        } catch (IOException ex) {
            Logger.getLogger(ApacheFileParser.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
