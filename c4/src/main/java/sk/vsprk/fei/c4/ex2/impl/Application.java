/*
 *  FEI prednaky 2016
 */
package sk.vsprk.fei.c4.ex2.impl;

import java.util.Scanner;
import javax.persistence.EntityManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.transaction.annotation.Transactional;
import sk.vsprk.fei.c4.ex1.impl.ApacheFileParser;

/**
 *
 * @author vsprk
 */
@SpringBootApplication
@ComponentScan(basePackages = {"sk.vsprk.fei.*"})
@EntityScan(value="sk.vsprk.fei") 
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class);
    }
    @Bean
    public CommandLineRunner demo(EntityManager em) {
        return new CommandLineRunner() {
            
            @Autowired
            MyService service;
            
            @Override
            @Transactional
            public void run(String... strings) throws Exception {
                System.out.println("Welcome to MyApp.");
                System.out.println("Available commands: ADD, ...");
                Scanner scanner = new Scanner(System.in);
                String command = null;
                do {
                    command = scanner.nextLine();
                    
                    switch (command) {
                        case "ADD" : {
                            System.out.println("Enter name");
                            String name = scanner.nextLine();
                            System.out.println("Enter surname");
                            String surname = scanner.nextLine();
                            service.add(name, surname); 
                            break;
                        }
                        default : {
                            System.out.println("unknown command " + command);
                        };
                    }
                } while (!command.equals("QUIT"));
                
            }
        };
    }
}
