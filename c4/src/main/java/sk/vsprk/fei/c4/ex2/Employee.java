package sk.vsprk.fei.c4.ex2;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Employee implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id;
    
    private String firstName;
    
    private String lastName;
    
    private Integer sallary;

    public Long getId() {
        return id;
    }

    protected Employee() {}
    
    public Employee(String firstName, String lastName, Integer sallary) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.sallary = sallary;
    }

    @Override
    public String toString() {
        return id + ": " + firstName + " " + lastName;
    }

    
    
}
