/*
 *  FEI prednaky 2016
 */
package sk.vsprk.fei.feicvika.c2.ex0;

import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;
import sk.vsprk.fei.feicvika.c2.ex2.Circle;

/**
 *
 * @author vsprk
 */
public class ShapeServiceTest {
    
    public ShapeServiceTest() {
    }

    /**
     * Test of getCircles method, of class ShapeService.
     */
    @Test
    public void testGetCircles() {
        System.out.println("getCircles");
        ShapeService instance = new ShapeService();
        List<Circle> expResult = null;
        List<Circle> result = instance.getCircles();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of add method, of class ShapeService.
     */
    @Test
    public void testAdd() {
        System.out.println("add");
        Circle circle = new Circle(10);
        ShapeService instance = new ShapeService();
        assertTrue(instance.getCircles().size() == 0);
        instance.add(circle);
        assertTrue(instance.getCircles().size() == 1);
    }

    /**
     * Test of sortCircles method, of class ShapeService.
     */
    @Test
    public void testSortCircles() {
        System.out.println("sortCircles");
        ShapeService instance = new ShapeService();
        instance.sortCircles();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of printCircles method, of class ShapeService.
     */
    @Test
    public void testPrintCircles() {
        System.out.println("printCircles");
        ShapeService instance = new ShapeService();
        instance.printCircles();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of create method, of class ShapeService.
     */
    @Test
    public void testCreate() {
        System.out.println("create");
        ShapeService instance = new ShapeService();
        instance.create();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
    
}
