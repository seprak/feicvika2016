/*
 *  FEI prednaky 2016
 */
package sk.vsprk.fei.feicvika.c2.ex2;

/**
 *
 * @author vsprk
 */
public class Rectangle {

    double width, height;

    public Rectangle(double width, double height) {
        this.width = width;
        this.height = height;
    }

    /**
     * Vypocita povrch obdlznika.
     *
     * @return number representing area of the rectangle.
     */
    public double getArea() {
        return width * height;
    }

}
