package sk.vsprk.fei.feicvika.c2.ex1;

/**
 *
 * @author vsprk
 */
public enum CarType {
    ELECTRIC, PETROL;
}
