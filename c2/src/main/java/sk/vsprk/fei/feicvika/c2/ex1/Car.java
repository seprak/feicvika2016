package sk.vsprk.fei.feicvika.c2.ex1;

import java.io.Serializable;
import javax.inject.Inject;

/**
 * basic car model.
 * 
 * @author vsprk
 */
public class Car implements Serializable {

    private int maxSpeed;
    
    private CarType carType;
    
    @Inject
    Engine engine;

    public int getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(int maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public CarType getCarType() {
        return carType;
    }

    public void setCarType(CarType carType) {
        this.carType = carType;
    }

    @Override
    public String toString() {
        return "Car with engine: " + engine;
    }

}
