/*
 *  FEI prednaky 2016
 */
package sk.vsprk.fei.feicvika.c2.ex2;

/**
 *
 * @author vsprk
 */
public class Circle {
    
    double r;

    public Circle(double r) {
        this.r = r;
    }
    
    public double getArea() {
        return r * r * Math.PI;
    }
}
