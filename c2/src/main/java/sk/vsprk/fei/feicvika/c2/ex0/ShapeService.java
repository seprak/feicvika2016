/*
 *  FEI prednaky 2016
 */
package sk.vsprk.fei.feicvika.c2.ex0;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import sk.vsprk.fei.feicvika.c2.ex2.Circle;

/**
 *
 * @author vsprk
 */
public class ShapeService {

    List<Circle> circleList = new ArrayList<>();

    public List<Circle> getCircles() {
        return circleList;
    }

    public void add(Circle circle) {
        circleList.add(circle);
    }

    public void sortCircles() {
        Collections.sort(circleList);
    }

    public void printCircles() {
        for (Circle circle : circleList) {
            System.out.println(circle);
        }
    }

    public void create() {
        for (int i = 0; i < 10; i++) {
            circleList.add(new Circle(Math.round(100 * Math.random())));
        }
    }
}
