/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.vsprk.fei.feicvika.c2.ex1.factory;

import javax.enterprise.inject.Produces;
import sk.vsprk.fei.feicvika.c2.ex1.Car;
import sk.vsprk.fei.feicvika.c2.ex1.annotation.SuperCarProducer;

/**
 *
 * @author vsprk
 */
public class SuperCarFactory {

    public @Produces @SuperCarProducer Car produceSupercar(Car car) {
        car.setMaxSpeed(300);
        return car;
    }
}
