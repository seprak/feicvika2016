# Doplnte hierarchiu tried pre cicavcov

## 1. Pridajte vseobecnu triedu pre psov (Pes/Dog) a konkretne plemena maciek (npr. Perzska) a psov (Doga, Vlciak).
- Skuste implementacne donutit programatora ktory vytvara triedu implementujucu zakladnu triedu
  Mammal aby implementoval metodu `public String makeSound()` (hint: abstract)

## 2. Urobte rozsirenie modelu tak aby:
- Nebolo potrebne aby konkretne plemeno implementovalo metodu makeSound()
- Pri zmene sposobu ozivania triedy Pes, tak sa zmeni aj sposob ozivania vsetkych plemien.

## 3. Zmente implementaciu triedy Mammal tak aby neobsahovala definiciu metody makeSound()
- hint: interface

## 4. Bez toho aby ste sa dotkli implementacie konkretnych tried, pridajte do modelu metodu ktora
umozni zveratam viacnasobne sa ohlasit.
- hint: `default` metoda.