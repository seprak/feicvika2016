/*
 *  FEI prednaky 2016
 */
package sk.vsprk.fei.feicvika.c2.ex0;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import sk.vsprk.fei.feicvika.c2.ex2.Circle;

/**
 *
 * @author vsprk
 */
public class MainCircel {

    public static void main(String[] args) {
        
        ShapeService shapeService = new ShapeService();
        shapeService.create();
        shapeService.printCircles();
        shapeService.sortCircles();
        shapeService.printCircles();
        
        
        
        List<Circle> circleList = new ArrayList<>();
        
        for (int i = 0; i < 10; i++) {
            circleList.add(new Circle(Math.round(100 * Math.random())));
        }

        for (Iterator iterator = circleList.iterator(); iterator.hasNext();) {
            Circle circle = (Circle) iterator.next();
            System.out.println(circle);
        }
        
        Collections.sort(circleList, new Comparator<Circle> (){

            @Override
            public int compare(Circle o1, Circle o2) {
                return -1*Double.compare(o1.getR(), o2.getR());
            }
        
        });
        System.out.println("********* SORTED *************");
        for (Circle circle : circleList) {
            System.out.println(circle);
        }
    }
}
