## Pridajte zavislost na projekte pre cvika �.1

- Pridajte spolocneho predka pre SuperCarFactory a ElectricCarFactory (npr. CarFactory)
- Implementujte metodu do CarManagera ktora vrati vsetky CarFactory  
Hint: `@Inject Instance<CarFactory> factories;`
- Napiste JUnit test ktory testuje pocet prvkov vo `factories`  
Hint: `javax.enterprise.inject extends Iterable`
