/*
 *  FEI prednaky 2016
 */
package sk.vsprk.fei.feicvika.c2.ex3;

/**
 *
 * @author vsprk
 */
public class Cat extends Mammal {
    
    public String makeSound() {
        return "Miau";
    }
}
