# Rozsirte hierarchiu tried

## 1. Pre zakladne geometricke tvary vytvorte spolocny *interface*

- ake metody su vhodne?
- pridajte definiciu metody ktora vrati matematicku rovnicu pre povrch.
- Naimplementujte definovanu metodu vo vsetkych implementaciach

## 2. Naimplementujte štvorec ako specialny pripad obdlznika

- HINT: `Square extends Rectangle`
- Potrebujeme nieco zmenit/overajdnut? (metodu na vypocet povrchu? konstruktor?)