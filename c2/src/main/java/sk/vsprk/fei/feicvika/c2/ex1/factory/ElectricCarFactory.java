/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.vsprk.fei.feicvika.c2.ex1.factory;

import javax.enterprise.inject.Produces;
import sk.vsprk.fei.feicvika.c2.ex1.Car;
import sk.vsprk.fei.feicvika.c2.ex1.CarType;
import sk.vsprk.fei.feicvika.c2.ex1.annotation.ElectricCarProducer;

/**
 *
 * @author vsprk
 */
public class ElectricCarFactory {
    
    @Produces @ElectricCarProducer Car produceCar(Car car) {
        car.setCarType(CarType.ELECTRIC);
        return car;
    }
}
