/*
 *  FEI prednaky 2016
 */
package sk.fei.feicvika.c3.ex2;

/**
 *
 * @author vsprk
 */
public interface Kalkulator {

    public int add(int a, int b);
    
    public int mul(int a, int b);
    
    public int div(int a, int b) throws DivisionByZeroException;
    
    public int sub(int a, int b);
}
