# Vytvorte triedu DriveService

- Trieda bude mat metodu "driveCar" ktora "da auto do pohybu"
  (vyjedrite tuto skutocnost implementacne. Hint: dalsia premenna instancie)
- Metoda driveCar si bude vediet poradit s necakanou udalostou (defekt, prazdna nadrz...)
- Vygenerujte JUnit test pre triedu Car a upravte ho tak aby testoval ci je auto v pohybe,
  a po zastaveni (turnOff) ci auto soji.
- Zmente implementaciu tak aby pouzivala vynimky namiesto navratovych hodnot.
- Zmente JUnit testy
