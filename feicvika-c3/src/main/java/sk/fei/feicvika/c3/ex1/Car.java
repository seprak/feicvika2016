package sk.fei.feicvika.c3.ex1;

public class Car {

    boolean flatTyre;
    boolean emptyTank;
    boolean majorFailure;

    public int[] drive() {
        int engineState = startEngine();
        int carState = moveCar();
        return new int [] {engineState, carState};
    }

    public boolean isFlatTyre() {
        return flatTyre;
    }

    public void setFlatTyre(boolean flatTyre) {
        this.flatTyre = flatTyre;
    }

    public boolean isEmptyTank() {
        return emptyTank;
    }

    public void setEmptyTank(boolean emptyTank) {
        this.emptyTank = emptyTank;
    }

    public boolean isMajorFailure() {
        return majorFailure;
    }

    public void setMajorFailure(boolean majorFailure) {
        this.majorFailure = majorFailure;
    }

    public void changeTyre() {
        flatTyre = false;
        System.out.println("Guma opravena...");
    }

    public void refuel() {
        emptyTank = false;
        System.out.println("Natankovane...");
    }

    public void turnOff() {
        System.out.println("Vypinam motor!");
        // TODO zmenit stav
    }

    private int startEngine() {
        if (emptyTank) {
            return 1;
        } else {
            return 0;
        }
    }

    private int moveCar() {
        if (flatTyre) {
            return 1;
        } else {
            return 0;
        }
    }
}
