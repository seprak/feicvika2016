# Implementujte interface Kalkulator

- Vytvorte main metodu ktora zoberie zo vstupu
  hodnoty x a y a vykona jednu z operacii (mul,div,sub,add)
  a toto vsetko v cykle pokim ako operaciu nezadam "end".
- Upravte implementaciu tak aby si kalkulator pamatal poslednu vypocitanu hodnotu.
- Vytvorte junit test pre triedu kalkulator
- implementujte metodu ktora prida prida funkcionalitu pre dve cisla na vstupe
  (a, b) -> ((a+b)/(a-b))
